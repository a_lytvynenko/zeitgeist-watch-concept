//var endpoint = 'http://172.28.88.171:3000/';
var endpoint = 'http://54.66.233.210/';
var newsRoot = 'http://smh.com.au/';
angular.module('starter.services', [])
.factory('dataProvider',['$http',function($http){
	return {
		getPlayLists : function(callback){
	      	if (window.localStorage.getItem('playlists')) {
            var items = JSON.parse(window.localStorage.getItem('playlists'));
            callback(items);
          }
          else
       {  

          $http.get(endpoint+'playlist/all',{ cache: true}).success(function(items){
	      	/*	if (window.localStorage && window.localStorage.getItem('playlists')) {
	      			var list = JSON.parse(window.localStorage.getItem('readLaterList'));
		      		items.forEach(function(playlist){
		      			playlist.tracks.forEach(function(track){
		      				track.readLater = list.indexOf(track._id)!==-1;
		      			});
		      		});

	      		} */
              window.localStorage.setItem('playlists',JSON.stringify(items));
	          	callback(items);
   		  	});
        } 
   		},
   		getReadLaterList:function(callback){
   		  	if (window.localStorage && window.localStorage.getItem('readLaterList')) {
	      		var list = JSON.parse(window.localStorage.getItem('readLaterList'));
			   	$http({	method:'POST',
			   			url:endpoint+'readlist',
			   			data:"list="+encodeURIComponent(list),
			   			headers:{'Content-Type': 'application/x-www-form-urlencoded'},
						options:{cache: true}})
			   	.success(function(items){
	   				console.log('READ LATER',items);
	   				callback(items);
		   		});	
		  	}
		  	else{
		  		callback([]);
		  	}
	
   		},
   		getSections:function(callback){
   			$http.get(endpoint+'sections',{ cache: true}).success(function(items){
   				callback(items);
   			});
   		},
   		getStory:function(uid,callback){
   			$http.get(endpoint+'track/'+uid,{cache:true}).success(function(story){
   				callback(story)
   			});
   		},

      getEntity:function(mid,callback){
        $http.get(endpoint+'entity/'+mid,{cache:true}).success(function(story){
          callback(story)
        });
      },
      checkMembership:function(email,pass,callback){
   			
   			$http.post(endpoint+'memberCheck',{email:email,pass:pass},{cache:true}).success(function(response){
   				callback(response)
   			}).error(function(){
   				callback({status:false});
   			});
   		}
   	}
}]);

angular.module('filters', []).factory('truncate', function () {
    return function strip_tags(input, allowed) {
      allowed = (((allowed || '') + '')
        .toLowerCase()
        .match(/<[a-z][a-z0-9]*>/g) || [])
        .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
      var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
      return input.replace(commentsAndPhpTags, '')
        .replace(tags, function($0, $1) {
          return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
        });
    }
});
