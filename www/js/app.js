// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','ngSanitize','ngCordova'])

.run(function($ionicPlatform,$rootScope,$ionicSlideBoxDelegate,$timeout,$ionicSideMenuDelegate,$cordovaPush,$cordovaLocalNotification) {

      
  $rootScope.allReady = false;
  $rootScope.readLaterList = [];
  $rootScope.sections = [];
  $rootScope.playAll = false;
  $rootScope.isMember = false;



  if (window.localStorage && window.localStorage.getItem('readLaterList')) {
      $rootScope.readLaterList = JSON.parse(window.localStorage.getItem('readLaterList'));
  }
 
   if (window.localStorage && window.localStorage.getItem('sections')) {
      $rootScope.sections = JSON.parse(window.localStorage.getItem('sections'));
  }
       var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
      $rootScope.windowHeight = windowHeight; 
      $rootScope.windowWidth = windowWidth;    


window.addEventListener("orientationchange", function(v) {
      var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
      $rootScope.windowHeight = windowHeight; 
      $rootScope.windowWidth = windowWidth;    
      $ionicSlideBoxDelegate.update();
      console.log('OR:',v);

}, false);

$rootScope.toggleMenu = function(){
  $ionicSideMenuDelegate.toggleRight();
}

$rootScope.checkDailyLimit = function(uid){
        var isMember = window.localStorage.getItem('isMember');
        var lastViewDate = window.localStorage.getItem('lastViewDate');
        var today = (new Date()).getDate()+'-'+((new Date()).getMonth()+1)+"-"+(new Date()).getFullYear();
        var viewedStories = window.localStorage.getItem('viewedStories');
        
         if (!isMember) {
           if (lastViewDate==today && viewedStories) {
              viewedStories = JSON.parse(viewedStories);
              if (viewedStories.length>2 && viewedStories.indexOf(uid)==-1) { return false; }
          }
          else
          {
             window.localStorage.removeItem('viewedStories');
            window.localStorage.setItem('lastViewDate',today);
          }
        }      
        return true;
     }

    $timeout(function(){
        $rootScope.allReady = true;
    },2000);


    //////PUSH NOTIFICATIONS///////




  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
      var iosConfig = {
      "badge":"true",
      "sound":"true",
      "alert":"true"
    };

    if(window.cordova) {
        setTimeout(function(){
   
              $cordovaPush.register(iosConfig).then(function(result) {
                console.log('$cordovaPush register success:',result);
              }, function(err) {
                console.log('$cprdovaPush register err:',err);
                  // An error occurred. Show a message to the user
              });

              $rootScope.$on('pushNotificationReceived', function(event, notification) {
                       
                       $cordovaPush.setBadgeNumber(notification.notification.badge).then(function(result) {
                        },
                        function(err) {
                          console.log("setBadgeNumber ERROR",err);
                        });
                    console.log('NOTIFICATION:',JSON.stringify(notification));  
              });

             //cordova.exec(function(s){console.log('PUSH REGISTER SUCCESS:',s)}, function(e){console.log('PUSH FAILURE:',e)}, "PushPlugin", "register", [iosConfig]);
            //cordova.exec(function(s){console.log('notificationReceived SUCCESS:',s)}, function(e){console.log('notificationReceived FAILURE:',e)}, "PushPlugin", "notificationReceived", []);

                   $cordovaLocalNotification.add({
                                                 id: 'ID'+(new Date()).getTime(),
                                                 date:       new Date(),    // This expects a date object
                                                 message:    "MESSAGE",  // The message that is displayed
                                                 title:      "TITLE",  // The title of the message
                                                 badge:      1,  // Displays number badge to notification
                                                 sound:      "ping.aiff",  // A sound to be played
                                                 json:       "{'data':{key:value}}",  // Data to be passed through the notification
                                                 }).then(function () {
                                                         console.log('callback for adding background notification');
                                                         });

                   
        },5000);
    }
    

    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

     if(AdMob) {
        var ad_units = {
        ios : {
            banner: '/6411/onl.adtester', // or DFP format "/6253334/dfp_example_ad"
            interstitial: '/6411/onl.adtester'
        },
        android : {
            banner: '/6411/onl.adtester', // or DFP format "/6253334/dfp_example_ad"
            interstitial: '/6411/onl.adtester'
        }
        };
        // select the right Ad Id according to platform
        $rootScope.admobid = ( /(android)/i.test(navigator.userAgent) ) ? ad_units.android : ad_units.ios;
       
        AdMob.prepareInterstitial( {adId:$rootScope.admobid.interstitial, autoShow:false} );
      }

    

    

 
      // An error occurred. Show a message to the user
  });
    console.log("PLATFORM READY PASSED");
  })

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })
    .state('app.story', {
      url: "/story/:uid",
      views: {
        'menuContent' :{
          templateUrl: "templates/story.html",
          controller: 'StoryCtrl'
        }
      }
    }).state('app.subscribe', {
      url: "/subscribe",
      views: {
        'menuContent' :{
          templateUrl: "templates/subscription.html",
          controller: 'SubscriptionCtrl'
        }
      }
    }).state('app.entity', {
      url: "/entity/:prefix/:mid",
      views: {
        'menuContent' :{
          templateUrl: "templates/entity.html",
          controller: 'EntityCtrl'
        }
      }
    })
      .state('app.readLater', {
      url: "/readlater",
      views: {
        'menuContent' :{
          templateUrl: "templates/readlater.html",
          controller: 'ReadLaterCtrl'
        }
      }
    })
    .state('app.playlists', {
      url: "/playlists",
      views: {
        'menuContent' :{
          templateUrl: "templates/playlists.html",
          controller: 'PlaylistsCtrl'
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
});

