angular.module('starter.controllers', ['starter.services','filters'])
.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  $scope.loginData = {};
 $scope.titlePosition=404;
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
 
  $scope.closeLogin = function() {
    $scope.modal.hide();
  },

  $scope.login = function() {
    $scope.modal.show();
  };

  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  }

})
.controller('PlaylistsCtrl', function($scope,dataProvider,$timeout,$ionicSlideBoxDelegate,$ionicScrollDelegate,$rootScope,$state) {
   
   $scope.itemHeight = 300;//($rootScope.windowHeight-44)/100*90;
   $scope.currentIndex = 0;
   $scope.isReady = false;

  $scope.loadPlaylists = function(){
    $rootScope.allReady = false;
     dataProvider.getSections(function(data){

        var currentSections = $rootScope.sections;
        if (currentSections.length==0) {
           $rootScope.sections = data;
           window.localStorage.setItem('sections',JSON.stringify(data));
        }
        else
        {

        }
       dataProvider.getPlayLists(function(data){
            var playlists = data;
            playlists.forEach(function(playlist){
              $rootScope.sections.forEach(function(section){
                  if (section.disabled) {
                    playlist.disabled = true;
                  }
              });
            });
            $rootScope.playlists = playlists;
            $rootScope.currentPlaylist = data[0];
            $scope.prevSecton = '';
            $scope.nextSection = $scope.playlists[1].title;
            $scope.sectionTitle = $scope.playlists[0].title;
            //$ionicSlideBoxDelegate.update();
           // $scope.$broadcast('scroll.refreshComplete');  
            $rootScope.allReady = true;
            console.log('ROOTSCOPE PLAYLISTS',$rootScope.playlists);
            $timeout(function(){
                $scope.isReady = true;
                $scope.$broadcast('scroll.refreshComplete');
            },300);
            
        });     
     });

 

    
  }
 
  $scope.play = function(item,idx){
    $rootScope.itemIdx = idx;
    $state.go('app.playlist');
    
  }

  $scope.openStory = function(uid,index) {
    //  var AdMob =  window.plugins ? AdMob : undefined;
     // $rootScope.playlists[$scope.currentIndex].tracks.forEach(function(track){track.active = false});
     
     // $rootScope.playlists[$scope.currentIndex].tracks[index].active = true;
     // $ionicScrollDelegate.scrollTo(0,index*300,true);



    if (!$rootScope.checkDailyLimit(uid)) {$state.go('app.subscribe'); return }

    if (window.cordova) {
     if (AdMob) {
        console.log("AD START");
        AdMob.prepareInterstitial( {adId:$rootScope.admobid.interstitial, autoShow:true} );
     
      }
    }

    $state.go('app.story',{uid:uid});
    
  }

$scope.playAll = function(){
    $rootScope.playAll = true;
    $state.go('app.playlist');
    
  }

  function beforeSlideChange(){
      $scope.prevSection = $scope.playlists[Math.max(0,$scope.currentIndex-1)].title;
      $scope.nextSection = $scope.playlists[Math.min($scope.playlists.length-1,$scope.currentIndex+1)].title;
      $scope.sectionTitle = $scope.playlists[$scope.currentIndex].title;
  }

  $scope.slideHasChanged = function(idx){
    console.log(idx);
    $timeout(function(){
      $rootScope.currentPlaylist = $rootScope.playlists[idx]; 
      $scope.currentIndex = idx;
      beforeSlideChange();  
    },300);

  }

  $scope.nextSlide = function(){
      $scope.currentIndex = (($scope.currentIndex+1) > $scope.playlists.length) ? 0 : $scope.currentIndex+1;
      beforeSlideChange();
      $ionicSlideBoxDelegate.slide($scope.currentIndex);
    
    }

  $scope.prevSlide = function(){
      $scope.currentIndex = ($scope.currentIndex==0) ? $scope.currentIndex = $scope.playlists.length-1 : $scope.currentIndex-1; 
      beforeSlideChange();
      $ionicSlideBoxDelegate.slide($scope.currentIndex);
  }




  $scope.addToFavorites = function(track){
    $rootScope.readLaterList.push(track._id);
    track.readLater = !track.readLater;
    window.localStorage.setItem('readLaterList',JSON.stringify($rootScope.readLaterList));
  }

  $scope.openReadLater = function(){
    $state.go('app.readLater');  
  }
  

  $scope.loadPlaylists();

})
.controller('StoryCtrl', function($scope, dataProvider,$stateParams,$rootScope,$ionicSlideBoxDelegate,$ionicScrollDelegate,$state,$timeout,$sce,$ionicNavBarDelegate,truncate,$ionicModal,$ionicLoading) {
  

 if (!$rootScope.checkDailyLimit($stateParams.uid)) {$state.go('app.subscribe'); return }

 var viewedStories = window.localStorage.getItem('viewedStories');
 if (viewedStories) {
   viewedStories = JSON.parse(viewedStories);
   if (viewedStories.indexOf($stateParams.uid)==-1) {
    viewedStories.push($stateParams.uid);
    window.localStorage.setItem('viewedStories',JSON.stringify(viewedStories));
   }
 }
 else
 {
  viewedStories = [];
  viewedStories.push($stateParams.uid);
  window.localStorage.setItem('viewedStories',JSON.stringify(viewedStories));
 } 

  $scope.itemHeight = ($rootScope.windowHeight-88)/2;
  var playlist = angular.copy($rootScope.currentPlaylist);
  if (!playlist) $state.go('app.playlists');
  $scope.currentIndex = 0;
  $scope.scrollText = false;
  $scope.activeTabIndex = 0;
  var storyId = $stateParams.uid;
  var storyIdx = playlist.trackIds.indexOf($stateParams.uid);
  var timeout = 0; 
  if (storyIdx!==-1) {
    var track = angular.copy(playlist.tracks[storyIdx]);
    track.arrTabs[0].data = '';
    $scope.story = track;
    $scope.text = $sce.trustAsHtml(truncate(track.html, '<p>'));
    $scope.gallery = [{title:"Title",src:track.image}];
    console.log(track.arrTabs);
    $scope.tabs = track.arrTabs;
    //$ionicSlideBoxDelegate.update();
    timeout = 500;
  }


  $timeout(function(){
    $ionicLoading.show({
       template: '<i class="ionicon ion-loading-d"></i>&nbsp;Fetching story...'
    });
    dataProvider.getStory($stateParams.uid,function(data){
          console.log(data);
          var track = data;
          track.arrTabs[0].data = '';
          $scope.story = track;
          $scope.text = $sce.trustAsHtml(truncate(track.html, '<p>'));
          $scope.gallery = track.gallery;
          $scope.tabs = timeout==0 ? track.arrTabs : $scope.tabs;
          console.log(track.gallery);
           for (var i=1;i<track.arrTabs.length;i++){
              $scope.tabs[i].data = track.arrTabs[i].data;  
           }
          //$ionicSlideBoxDelegate.update();   
     
       $ionicSlideBoxDelegate.$getByHandle('image-slides').update();
      $ionicLoading.hide();
    });
  },timeout);


  $scope.toggleTab = function(index){
        $ionicSlideBoxDelegate.$getByHandle('story-tabs').slide(index); 
        $scope.activeTabIndex = index;
  }


/*$scope.onStoryContentScroll = function(evt) {
  var titlePosition = document.getElementById('story-title').getBoundingClientRect().bottom;
  var imageShift = (404-titlePosition);
  document.getElementById('story-gallery').style["-webkit-transform"] = "translateY("+(imageShift/5)+"px)";

  //evt.stopPropagation();
}*/


$scope.tabHasChanged = function(index) {
  $scope.activeTabIndex = index;

}
/*
$scope.onTextContentScroll = function(evt) {
  var titlePosition = document.getElementById('story-title').getBoundingClientRect().bottom;
  if (titlePosition<=111 && $scope.scrollText==false) {
    $scope.scrollText = true;
  }
}*/


$scope.goBack = function() {
    $ionicNavBarDelegate.back();
  };


  $scope.addToFavorites = function(track){
    $rootScope.readLaterList.push(track._id);
    track.readLater = !track.readLater;
    window.localStorage.setItem('readLaterList',JSON.stringify($rootScope.readLaterList));
  }


  $scope.openEntity = function(type,uid){
    $ionicModal.fromTemplateUrl('story.html',{
      scope:$scope,
      animation:'slide-in-up'
    }).then(function(modal){
      $scope.modal = modal;
      $scope.modal.show();
    });
  }

  $scope.openItem = function(href) {
    var href = href[0]=='/' ? href.substring(1) : href;
    window.location.href = href;

  }

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
  


})
.controller('EntityCtrl', function($scope, dataProvider,$stateParams,$rootScope,$ionicSlideBoxDelegate,$ionicScrollDelegate,$state,$timeout,$sce,$ionicNavBarDelegate,truncate,$ionicModal,$ionicLoading) {

    var mid = $stateParams.prefix+'/'+$stateParams.mid;
    $timeout(function(){
    $ionicLoading.show({
       template: '<i class="ionicon ion-loading-d"></i>&nbsp;Fetching data...'
    });
    dataProvider.getEntity(mid,function(data){
          $scope.entity = data;
          $scope.text = $sce.trustAsHtml(truncate(data.html, '<p>'));
          $scope.gallery = data.gallery;
          $scope.tabs=[];
          var cnt=0;
           for (var i in data.tabs){
              $scope.tabs[cnt] = data.tabs[i];
              cnt++;  
           }
          //$ionicSlideBoxDelegate.update();   
     
       $ionicSlideBoxDelegate.$getByHandle('image-slides').update();
      $ionicLoading.hide();
    });
  },0);

  $scope.toggleTab = function(index){
        $ionicSlideBoxDelegate.$getByHandle('story-tabs').slide(index); 
        $scope.activeTabIndex = index;
  }

  $scope.goBack = function() {
    $ionicNavBarDelegate.back();
  };

  $scope.openItem = function(href) {
    var href = href[0]=='/' ? href.substring(1) : href;
    window.location.href = href;

  }

})
.controller('ReadLaterCtrl', function($scope,dataProvider,$timeout,$ionicSlideBoxDelegate,$rootScope,$state,$ionicNavBarDelegate) {
  
  $scope.playlists = [];
  $scope.itemHeight = ($rootScope.windowHeight-44)/2;
   $scope.currentIndex = 0;
 



  $scope.refresh = function(){
    $rootScope.allReady = false;
     dataProvider.getReadLaterList(function(data){
         $scope.playlists = [data];
          $rootScope.playlists = [data];
          $rootScope.currentPlaylist = data;
          //$ionicSlideBoxDelegate.update();
          //$scope.$broadcast('scroll.refreshComplete');  
          $rootScope.allReady = true;
         
      });
  }
 
  $scope.play = function(item,idx){
    console.log(item);
    $rootScope.itemIdx = 0;
    $state.go('app.playlist');  
  }

  $scope.goBack = function() {
    $state.go('app.playlists');  
  };

  $scope.refresh();


})
.controller('MenuCtrl', function($scope,dataProvider,$timeout,$ionicSlideBoxDelegate,$rootScope,$state,$ionicNavBarDelegate) {

  
          $scope.data = {
            showDelete: false
          };
          
          $scope.moveItem = function(item, fromIndex, toIndex) {
            $rootScope.sections.splice(fromIndex, 1);
            $rootScope.sections.splice(toIndex, 0, item);
            window.localStorage.setItem('sections',JSON.stringify($rootScope.sections));
          };
          
          $scope.onItemDelete = function(item) {
            $rootScope.sections.splice($rootScope.sections.indexOf(item), 1);
            window.localStorage.setItem('sections',JSON.stringify($rootScope.sections));
          };
          
          $scope.disableSection = function(item) {
           
            item.disabled = !item.disabled;
            window.localStorage.setItem('sections',JSON.stringify($rootScope.sections));
          };
})
.controller('SubscriptionCtrl', function($scope,dataProvider,$timeout,$rootScope,$state,$ionicNavBarDelegate,$ionicLoading,$ionicPopup) {
    
    $scope.errors = {}; 
    $scope.email = "alex.lytvynenko@fairfaxmedia.com.au";
    $scope.pass = "qwerty";
    $scope.checkSubsription = function(){
      console.log($scope.email,$scope.pass);
   //   if (!loginForm.email.valid) {$scope.errors.email = true; return;}
   //   if (!loginForm.pass.valid) {$scope.errors.pass = true; return;}
      $ionicLoading.show({
      template: 'Checking your membership...'
      });

      dataProvider.checkMembership($scope.email,$scope.pass,function(response){
          $ionicLoading.hide();
          if (response && response.status == true) {
        
               $scope.loginProcessing = false;
               $rootScope.isMember = true;
               window.localStorage.setItem('isMember',true);
               $ionicPopup.alert({
                 title: 'THANK YOU!',
                 template: 'Now you have unlimited access to Zeitgeist!'
               }).then(function(res) {
                 $state.go('app.playlists'); 
               });
             }
      });

    }

    $scope.resetValidation = function(field){
      $scope.errors[field] = false;
    }

      $scope.goBack = function() {
      $state.go('app.playlists');
    };

})